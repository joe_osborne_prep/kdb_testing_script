dates: 2015.01.05+1000000?59
sdates: asc dates

times:1000000?24:00:00.000000000
stimes: asc times

syms: 1000000?`BT`VOD`BP`ADM`RR
vols:10*1+1000000?1000
price:90+(1000000?6001)%100
trades:([] date:sdates; time:stimes; sym:syms; vol:vols; price:price)

trades: update price:4*price from trades where sym=`BP
trades: update price:2*price from trades where sym=`BT
trades: update price:6*price from trades where sym=`VOD
trades: update price:3*price from trades where sym=`RR

a:select avg price,avg vol by sym from trades
b:select min_price:min price, max_price:max price by sym from trades
c:select vwap:vol wavg price by sym,bkt:10000000000 xbar time from trades /every 10 mins
d:select vwap:vol wavg price by sym,bkt:3600000000000 xbar time from trades / every hour
e:{select max price by sym from trades}
f:{select (|/) price by sym from trades}
g:{select 1#date, 1#time, 1#vol, (|/) price, by sym from trades }

